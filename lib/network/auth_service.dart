import 'package:injectable/injectable.dart';
import 'package:roonyx_test/network/models/login_response.dart';
import 'package:dart_either/dart_either.dart';

abstract class AuthService {
  Future<Either<String, LoginResponse>> login(String email, String password);
}

@Singleton(as: AuthService)
class AuthServiceImpl extends AuthService {
  @override
  Future<Either<String, LoginResponse>> login(
      String email, String password) async {
    // note: simulate some api call
    await Future.delayed(Duration(seconds: 1));
    switch (email) {
      case "a@mail.com":
        return const Either.left("Duplicate email");
      case "b@mail.com":
        return const Either.left("Invalid email");
      default:
        return Either.right(LoginResponse(email: email));
    }
  }
}
