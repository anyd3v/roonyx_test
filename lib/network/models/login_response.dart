class LoginResponse {
  final String email;

  LoginResponse({
    required this.email,
  });
}
