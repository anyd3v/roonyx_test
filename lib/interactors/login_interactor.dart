import 'package:dart_either/dart_either.dart';
import 'package:injectable/injectable.dart';
import 'package:roonyx_test/network/auth_service.dart';

abstract class LoginInteractor {
  Future<Either<String, void>> login(String email, String password);
}

@Singleton(as: LoginInteractor)
class LoginInteractorImpl extends LoginInteractor {
  final AuthService _authService;

  LoginInteractorImpl({
    required AuthService authService,
  }) : _authService = authService;

  @override
  Future<Either<String, void>> login(
    String email,
    String password,
  ) async {
    final response = await _authService.login(email, password);

    // note: run additional login such as save auth token to preferences etc

    return response.map((value) {});
  }
}
