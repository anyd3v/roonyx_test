import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:go_router/go_router.dart';
import 'package:roonyx_test/extentions/context.dart';
import 'package:roonyx_test/models/email_error.dart';
import 'package:roonyx_test/models/password_error.dart';
import 'package:roonyx_test/routes.dart';
import 'package:roonyx_test/screens/login/cubit.dart';
import 'package:roonyx_test/screens/login/state.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _emailTextController = TextEditingController();
  final _passwordTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final localization = context.localization();

    return Scaffold(
        appBar: AppBar(
          title: Text(localization.login_screen_toolbar),
        ),
        body: BlocProvider<LoginCubit>(
          create: (context) => GetIt.I.get<LoginCubit>(),
          child: BlocConsumer<LoginCubit, LoginState>(
            listener: (context, state) {
              if (state.successful) {
                context.replace(mainRoute);
              }
            },
            builder: (context, state) => Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 32,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      controller: _emailTextController,
                      decoration: InputDecoration(
                        hintText: localization.email,
                        errorText: _emailError(
                              state.emailError,
                              localization,
                            ) ??
                            state.error,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      controller: _passwordTextController,
                      decoration: InputDecoration(
                        hintText: localization.password,
                        errorText: _passwordError(
                          state.passwordError,
                          localization,
                        ),
                      ),
                      obscureText: true,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    if (state.processing)
                      CircularProgressIndicator()
                    else
                      ElevatedButton(
                        onPressed: () {
                          context.read<LoginCubit>().login(
                                _emailTextController.text,
                                _passwordTextController.text,
                              );
                        },
                        child: Text(localization.login),
                      )
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  String? _emailError(EmailError error, AppLocalizations localizations) {
    switch (error) {
      case EmailError.NONE:
        return null;
      case EmailError.MANDATORY:
        return localizations.mandatory_field;
      case EmailError.FORMAT:
        return localizations.email_format;
    }
  }

  String? _passwordError(PasswordError error, AppLocalizations localizations) {
    switch (error) {
      case PasswordError.NONE:
        return null;
      case PasswordError.MANDATORY:
        return localizations.mandatory_field;
      case PasswordError.FORMAT:
        return localizations.password_format;
    }
  }

  @override
  void dispose() {
    _emailTextController.dispose();
    _passwordTextController.dispose();
    super.dispose();
  }
}
