import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:roonyx_test/models/email_error.dart';
import 'package:roonyx_test/models/password_error.dart';

part 'state.g.dart';

@CopyWith()
class LoginState {
  final EmailError emailError;
  final PasswordError passwordError;
  final String? error;
  final bool processing;
  final bool successful;

  LoginState({
    required this.emailError,
    required this.passwordError,
    this.error,
    required this.processing,
    required this.successful,
  });
}
