import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:roonyx_test/interactors/login_interactor.dart';
import 'package:roonyx_test/models/email_error.dart';
import 'package:roonyx_test/models/password_error.dart';
import 'package:roonyx_test/screens/login/state.dart';
import 'package:roonyx_test/validators/email_validator.dart';
import 'package:roonyx_test/validators/password_validator.dart';

@Injectable()
class LoginCubit extends Cubit<LoginState> {
  final EmailValidator _emailValidator;
  final PasswordValidator _passwordValidator;
  final LoginInteractor _loginInteractor;

  LoginCubit({
    required EmailValidator emailValidator,
    required PasswordValidator passwordValidator,
    required LoginInteractor loginInteractor,
  })  : _emailValidator = emailValidator,
        _passwordValidator = passwordValidator,
        _loginInteractor = loginInteractor,
        super(LoginState(
          emailError: EmailError.NONE,
          passwordError: PasswordError.NONE,
          processing: false,
          successful: false,
        ));

  void login(String email, String password) async {
    if (!state.processing) {
      emit(state.copyWith(processing: true));

      final emailError = _emailValidator.isValid(email);
      final passwordError = _passwordValidator.isValid(password);
      emit(state.copyWith(
        emailError: emailError,
        passwordError: passwordError,
      ));

      if (emailError == EmailError.NONE &&
          passwordError == PasswordError.NONE) {
        emit(state.copyWith(error: null));
        final response = await _loginInteractor.login(email, password);
        response.fold(
          ifLeft: (error) {
            emit(state.copyWith(processing: false, error: error));
          },
          ifRight: (response) {
            emit(state.copyWith(successful: true));
          },
        );
      } else {
        emit(state.copyWith(processing: false));
      }
    }
  }
}
