import 'package:flutter/material.dart';
import 'package:roonyx_test/extentions/context.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final localization = context.localization();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          localization.main_screen_toolbar,
        ),
      ),
      body: Center(
        child: Text(
          localization.main_screen_body,
        ),
      ),
    );
  }
}
