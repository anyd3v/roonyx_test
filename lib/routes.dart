import 'package:go_router/go_router.dart';
import 'package:roonyx_test/screens/login/screen.dart';
import 'package:roonyx_test/screens/main/screen.dart';

const loginRoute = '/';
const mainRoute = '/main';

final router = GoRouter(
  initialLocation: loginRoute,
  routes: [
    GoRoute(
      path: loginRoute,
      builder: (context, state) => const LoginScreen(),
    ),
    GoRoute(
      path: mainRoute,
      builder: (context, state) => const MainScreen(),
    ),
  ],
);
