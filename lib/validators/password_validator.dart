import 'package:injectable/injectable.dart';
import 'package:roonyx_test/models/password_error.dart';

abstract class PasswordValidator {
  PasswordError isValid(String password);
}

@Singleton(as: PasswordValidator)
class PasswordValidatorImpl extends PasswordValidator {
  @override
  PasswordError isValid(String password) {
    if (password.isEmpty) {
      return PasswordError.MANDATORY;
    } else {
      // note: just to simplify password checking
      final isValid = password.length > 5;
      if (isValid) {
        return PasswordError.NONE;
      } else {
        return PasswordError.FORMAT;
      }
    }
  }
}
