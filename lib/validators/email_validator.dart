import 'package:injectable/injectable.dart';
import 'package:email_validator/email_validator.dart' as ev;
import 'package:roonyx_test/models/email_error.dart';

abstract class EmailValidator {
  EmailError isValid(String email);
}

@Singleton(as: EmailValidator)
class EmailValidatorImpl extends EmailValidator {
  @override
  EmailError isValid(String email) {
    if (email.isEmpty) {
      return EmailError.MANDATORY;
    } else {
      final isValid = ev.EmailValidator.validate(email);
      if (isValid) {
        return EmailError.NONE;
      } else {
        return EmailError.FORMAT;
      }
    }
  }
}
